The document repository is part of a tutorial by **0612 TV** entitled **Partial Application - Friday Minis 314**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/5sdDnBdb_Is/0.jpg)](https://www.youtube.com/watch?v=5sdDnBdb_Is "Click to Watch")

------

# A simple `add()` function


```python
def add(a,b):
    return a+b
```


```python
add(5,3)
```




    8



# An `add()` function in two parts

Notice how `add_partial()` **returns a function**!


```python
def add_partial(a):
    def nextpart(b):
        return a+b
    return nextpart
```

Thus, calling `add_partial()` creates a function:


```python
add_partial(2)
```




    <function __main__.add_partial.<locals>.nextpart(b)>



We can call that returned function to complete the calculation:


```python
addtwo = add_partial(2)
addtwo(5)
```




    7



We can also call the function "all together". Note that this is really _two function calls_, the first of which returning a function, which is immediately called by the second set of parameters.


```python
add_partial(3)(8)
```




    11



# Partial Applications for Code Reuse

A partially-applied function may be reused multiple times if necessary


```python
addtwo = add_partial(2)
print(addtwo(3))
print(addtwo(5))
print(addtwo(8))
```

    5
    7
    10
    

# Automatically Build Partial-Application Functions!

The following code "converts" any normal _binary_ function (ie. Takes in two inputs) into a partially-applied function.


```python
def bin_to_partial(f):
    def intermediate_fn(a):       # Create an intermediate function that takes in the 1st param
        def final_fn(b):          # This function returns another function that takes in the 2nd param
            return f(a,b)         #     the innermost function calls the original with the two values
        return final_fn
    return intermediate_fn
```


```python
def mul(a,b):
    return a*b
```

We start by converting `mul(a,b)` to a partially-applied function, using `bin_to_partial()`.


```python
mul_partial = bin_to_partial(mul)
```

Now, we can use `mul_partial()` like any partially-applied function!


```python
timesthree = mul_partial(3)
timesthree(5)
```




    15



# Currying

Currying is slightly stricter in that each "intermediate" function takes in only one parameter, regardless of how many parameters are required in total.


```python
def curried_triple_add(a):
    def fn1(b):
        def fn2(c):
            return a+b+c
        return fn2
    return fn1
```


```python
curried_triple_add(1)(2)(3)
```




    6


